const btnChange = document.querySelector('.btn');

const navMenu = document.querySelector('.change1');
const content1 = document.querySelector('.change2');
const themeValue = localStorage.getItem('newTheme');

if (themeValue) {
  document.body.classList.add(themeValue);
  navMenu.classList.add('rose-theme');
  content1.classList.add('rose-theme')
}

btnChange.addEventListener('click', () => {
  if (!localStorage.getItem('newTheme')) {
    localStorage.setItem('newTheme', 'rose');
    navMenu.classList.remove ('red-theme');
    navMenu.classList.add('rose-theme');
    content1.classList.add('rose-theme');
  } else {
    localStorage.removeItem('newTheme');
    navMenu.classList.remove ('rose-theme');
    navMenu.classList.add('red-theme');
    content1.classList.remove('rose-theme');

  }
})

